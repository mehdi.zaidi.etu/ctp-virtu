# Mehdi ZAIDI FRENE25 groupe G

FROM debian:bullseye
RUN apt-get update && apt-get install -y python3 wget unzip && rm -r /var/lib/apt/lists/*
WORKDIR /src
EXPOSE 8000
RUN wget https://gitlab.univ-lille.fr/michael.hauspie/webcalc/-/archive/v0.1/webcalc-v0.1.zip
RUN unzip webcalc-v0.1.zip && mv webcalc-v0.1 webcalc-source && rm webcalc-v0.1.zip
WORKDIR /src/webcalc-source
CMD ["/src/webcalc-source/main.py"]
ENTRYPOINT ["/usr/bin/python3"]

## VERSION 2 :
## Permet de supprimer quelques dépendances (wget et unzip) qui ne sont pas utile pour utiliser l'application.
## L'utilisation reste identique.

# FROM debian:bullseye AS source
# RUN apt-get update && apt-get install -y wget unzip && rm -r /var/lib/apt/lists/*
# WORKDIR /src
# RUN wget https://gitlab.univ-lille.fr/michael.hauspie/webcalc/-/archive/v0.1/webcalc-v0.1.zip
# RUN unzip webcalc-v0.1.zip && mv webcalc-v0.1 webcalc-source && rm webcalc-v0.1.zip

# FROM debian:bullseye
# RUN apt-get update && apt-get install -y python3 && rm -r /var/lib/apt/lists/*
# COPY --from=source /src/webcalc-source /src/webcalc-source/
# EXPOSE 8000
# WORKDIR /src/webcalc-source
# CMD ["/src/webcalc-source/main.py"]
# ENTRYPOINT ["/usr/bin/python3"]