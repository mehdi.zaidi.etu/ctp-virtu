# Mehdi ZAIDI FRENE25 groupe G

## Usage
1. Build the image with `sudo docker build -t webcalc .`
2. Start a container with `sudo docker run --rm -d --name webcalc -p 8000:8000 webcalc`
3. Send an http request to `localhost:8000` to do maths
> exemple : `curl -X POST localhost:8000 -H 'Content-Type: application/json' -d '{"operation": "PLUS", "op1": 3, "op2": 4}'`

4. When you're done, `sudo docker container kill webcalc` to stop it.

To execute tests : `sudo docker run --rm -it webcalc tests.py`